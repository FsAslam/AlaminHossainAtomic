<?php

//include_once ($_SERVER['DOCUMENT_ROOT'].DIRECTORY_SEPARATOR.'Atomic'.DIRECTORY_SEPARATOR.'views'.DIRECTORY_SEPARATOR.'SEIP107413'.DIRECTORY_SEPARATOR.'startup.php');
include_once ('../../../vendor/autoload.php');

use \App\Bitm\SEIP107413\Subcription\Email;
use \App\Bitm\SEIP107413\Utility\Utility;
 

$obj = new Email();
$email = $obj->show($_GET['id']);

?>

<html>
    <head>
        <title>Single View</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <style>
            #utility{
                float: right;
                width: 60%;
            }
            #message{
                background-color: blue;
            }
        </style>
    </head>
    <body>
<h1>Book Detail</h1>

<dl>
    <dt>Id</dt>
    <dd><?php echo $email['id']; ?></dd>
    
    <dt>Title</dt>
    <dd><?php echo $email['name']; ?></dd>
    
    <dt>Author</dt>
    <dd><?php echo $email['email']; ?></dd>
</dl>

<nav>
    <li> <a href="index.php">Go to list</a></li>
</nav>

    </body>
</html>