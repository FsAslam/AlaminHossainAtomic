<?php

include_once ('../../../vendor/autoload.php');

use App\Bitm\SEIP107413\Human\Gender;
use \App\Bitm\SEIP107413\Utility\Utility;
 

$obj = new Gender();
$gen = $obj->show($_GET['id']);

?>

<html>
    <head>
        <title>Single View</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <style>
            #utility{
                float: right;
                width: 60%;
            }
            #message{
                background-color: blue;
            }
        </style>
    </head>
    <body>
<h1>User Detail</h1>

<dl>
    <dt>Id</dt>
    <dd><?php echo $gen['id']; ?></dd>
    
    <dt>Name</dt>
    <dd><?php echo $gen['name']; ?></dd>
    
    <dt>Gender</dt>
    <dd><?php echo $gen['gender']; ?></dd>
</dl>

<nav>
    <li> <a href="index.php">Go to list</a></li>
</nav>

    </body>
</html>