<?php
include_once ('../../../vendor/autoload.php');

use \App\Bitm\SEIP107413\Agreement\TermAndCon;
use \App\Bitm\SEIP107413\Utility\Utility;
 
$obj = new TermAndCon();
$terms = $obj->index();

?>

<!DOCTYPE html>
<html>
    <head>
        <link rel="stylesheet" href="../../../resource/css/bootstrap.min.css" type="text/css"/>
        <link rel="stylesheet" href="../../../resource/css/bootstrap.css" type="text/css">


        <title>List Our Candidate</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        
        <style>
            #utility{
                float: right;
                width: 60%;
            }
            #message{
                background-color: blue;
            }
        </style>
        
        <script src="../../../resource/js/jquery-1.11.3.min.js"></script>
        <script src="../../../resource/js/bootstrap.min.js"></script>
    </head>
    <body>
      
        <nav class="navbar navbar-inverse">
            <div class="container-fluid">
                <div class="navbar-header">
                    <div class="col-md-offset-11">
                        <a class="navbar-brand" href="#">View</a>
                    </div>
                </div> 
            </div>
        </nav>
        <div class="row">
            <!--        <div class="col-sm-1"></div>-->
            <div class="col-sm-2">
                <a href="create.php" class="btn btn-primary">Add</a>
            </div>
            <div class="col-sm-3"> <div class="container">
                    <div class="dropdown">
                        <button class="btn btn-primary dropdown-toggle" type="button" data-toggle="dropdown">Show pages
                            <span class="caret"></span></button>
                        <ul class="dropdown-menu">
                            <li><a href="#">10</a></li>
                            <li><a href="#">20</a></li>
                            <li><a href="#">30</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <!--        <div class="col-sm-2"></div>-->
            <div class="col-sm-4">
                <span class="active">Download as pdf | XL</span>
            </div>
        </div>
        <div class="container-fluid">
            <div class="col-lg-offset-2">
                
                <table class="table table-hover  ">
                    <thead>
                        <tr class="info">
                            <th >SerialNumber</th>
                            <th>Id</th>
                            <th >Name &dArr;</th>
                            <th>Agreement &dArr;</th>
                            <th>Action</th>
                        </tr>
                    </thead>
                    <tbody>
                         <?php
                        $serialNumber = 1;
                        foreach ($terms as $term) {
                            ?>
                            <tr>
                                <td><?php echo $serialNumber; ?></td>
                                <td><?php echo $term['id'];?></td>
                                <td><a href="show.php?id=<?php echo $term['id']; ?>"><?php echo $term['name']; ?></a></td>
                                <td><?php echo $term['agree']; ?></td>
                                <td>
                                    
                                    <a href="show.php?id=<?php echo $term['id'];?>">View</a>
                                    <a href="edit.php?id=<?php echo $term['id'];?>">Edit</a>
                                    <a href="delete.php?id=<?php echo $term['id'];?>" class="delete">Delete</a>
                                    
                                    <form action="delete.php" method="post">
                                        <input type="hidden" name="id" value="<?php echo $term['id'];?>">
                                        <button type="submit" class="delete">Delete </button>
                                            
                                    </form>
<!--                                    <div class="container">
                                        <a href="show.php"> <button type="button" class="btn btn-success">View</button></a>
                                        <button type="button" class="btn btn-primary">Edit</button>
                                        <form action="delete.php" method="POST">
                                            <input type="hidden" name="id" value="<?php echo $book->id; ?>">
                                        <button type="submit" class="btn btn-danger delete">Delete</button>
                                        <a href="delete.php?id=<?php echo $book->id; ?>" class="delete">Delete</a>
                                                        </form>

                                        <button type="button" class="btn btn-info">Trash/Recover</button>
                                        <button type="button" class="btn btn-warning">Email to friend</button>
                                    </div>-->

                                    <!--View | Edit | Delete | Trash/Recover | Email to Friend-->
                                </td>
                            </tr>
                            <?php
                                $serialNumber++;
                            }
                            ?>
<!--                        <tr>
                            <td>Mary</td>
                            <td>Moe</td>
                            <td><div class="container">
                                    <button type="button" class="btn btn-success">View</button>
                                    <button type="button" class="btn btn-primary">Edit</button>
                                    <button type="button" class="btn btn-danger">Delete</button>
                                    <button type="button" class="btn btn-info">Trash/Recover</button>
                                    <button type="button" class="btn btn-warning">Email to friend</button>
                                </div>
                            </td>
                        </tr>
                        <tr>
                            <td>July</td>
                            <td>Dooley</td>
                            <td><div class="container">
                                    <button type="button" class="btn btn-success">View</button>
                                    <button type="button" class="btn btn-primary">Edit</button>
                                    <button type="button" class="btn btn-danger">Delete</button>
                                    <button type="button" class="btn btn-info">Trash/Recover</button>
                                    <button type="button" class="btn btn-warning">Email to friend</button>
                                </div>
                            </td>
                        </tr>-->
                    </tbody>
                </table>
            </div>

        </div>
        <ul class="pager">   
            <li><a href="#">Previous</a></li>
            <ul class="pagination pagination-lg">
                <li><a href="#">1</a></li>
                <li><a href="#">2</a></li>
                <li><a href="#">3</a></li>
                <li><a href="#">4</a></li>
                <li><a href="#">5</a></li>
            </ul>
            <li><a href="#">Next</a></li>
        </ul>
        
        <script>
            $('.delete').bind('click',function (e){
                var deleteItem = confirm("Are you sure to delete");
                if(!deleteItem){
//                    return false;
                     e.preventDefault();
                }
            });
            
            $('#message').hide('100'); 
        </script>
    </body>
</html>




