
   <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
        <title></title>
        <meta name="description" content="">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="apple-touch-icon" href="apple-touch-icon.png">

        <link rel="stylesheet" href="resource/css/bootstrap.min.css">
        <style>
            body {
                padding-top: 50px;
                padding-bottom: 20px;
            }
        </style>
        <link rel="stylesheet" href="resource/css/bootstrap-theme.min.css">
        <link rel="stylesheet" href="resource/css/main.css">

        <script src="js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
    </head>
    <body>
        <!--[if lt IE 8]>
            <p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
        <![endif]-->
<!--    <nav class="navbar navbar-inverse navbar-fixed-top" role="navigation">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Project name</a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <form class="navbar-form navbar-right" role="form">
            <div class="form-group">
              <input type="text" placeholder="Email" class="form-control">
            </div>
            <div class="form-group">
              <input type="password" placeholder="Password" class="form-control">
            </div>
            <button type="submit" class="btn btn-success">Sign in</button>
          </form>
        </div>/.navbar-collapse 
      </div>
    </nav>-->

    <!-- Main jumbotron for a primary marketing message or call to action -->
    <div class="jumbotron">
      <div class="container" align ="center">
          <h1 class="bg-primary" >BITM - Web app dev - PHP</h1>
          <dl>
            <dt><span>Name:</span></dt>
            <dd>Alamin Hossain</dd>
            
            <dt>SEIP ID:</dt>
            <dd>SEIP107413</dd>
            
            <dt>Batch:</dt>
            <dd>B10</dd>
        </dl>
<!--        <p><a class="btn btn-primary btn-lg" href="#" role="button">Learn more &raquo;</a></p>-->
      </div>
    </div>

    <div class="container">
        <h2>Projects</h2>
        <div class="table-responsive">
        <table class="table table-hover">
            <thead>
                <tr class="success">
                    <th>Sl.</th>
                    <th>Project Name</th>
                </tr>
            </thead>
            <tbody>
                <tr class="active">
                    <td>01</td>
                    <td><a href="views/SEIP107413/Birthday//index.php">Birthday</a></td>
                </tr>
                <tr class="warning">
                    <td>03</td>
                    <td><a href="views/SEIP107413/BookTitle/index.php">Book Title</a></td>
                </tr>
                <tr class="warning">
                    <td>02</td>
                    <td><a href="views/SEIP107413/City//index.php">City</a></td>
                </tr>
                
                 <tr class="warning">
                     <td>04</td>
                     <td><a href="views/SEIP107413/Gender//index.php">Gender</a></td>
                </tr>
                 <tr class="warning">
                    <td>05</td>
                    <td><a href="views/SEIP107413/Hobby//index.php">Hobby</a></td>
                </tr>
                <tr class="warning">
                    <td>06</td>
                    <td><a href="views/SEIP107413/ProfilePic//index.php">Profile Picture</a></td>
                </tr>
                 <tr class="warning">
                    <td>07</td>
                    <td><a href="views/SEIP107413/Email//index.php">Subcription</a></td>
                </tr>
                 
                 <tr class="warning">
                    <td>08</td>
                    <td><a href="views/SEIP107413/Summary/index.php">Summary</a></td>
                </tr>
                 <tr class="warning">
                    <td>09</td>
                    <td><a href="views/SEIP107413/TermAndCon/index.php">Term&Codition</a></td>
                </tr>
                 
            </tbody>
        </table>
        </div>
      

      <hr>

      <footer>
        <p align="center">&copy; Fs Aslam Shah</p>
      </footer>
    </div> <!-- /container -->        <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.js"></script>
        <script>window.jQuery || document.write('<script src="js/vendor/jquery-1.11.2.js"><\/script>')</script>

        <script src="js/vendor/bootstrap.min.js"></script>

        <script src="js/main.js"></script>

        <!-- Google Analytics: change UA-XXXXX-X to be your site's ID. -->
        <script>
            (function(b,o,i,l,e,r){b.GoogleAnalyticsObject=l;b[l]||(b[l]=
            function(){(b[l].q=b[l].q||[]).push(arguments)});b[l].l=+new Date;
            e=o.createElement(i);r=o.getElementsByTagName(i)[0];
            e.src='//www.google-analytics.com/analytics.js';
            r.parentNode.insertBefore(e,r)}(window,document,'script','ga'));
            ga('create','UA-XXXXX-X','auto');ga('send','pageview');
        </script>
    </body>
</html>

