<?php

namespace App\Bitm\SEIP107413\Utility;

class Utility{
    
    public $message ="";
    static public function Debug($parameter =false){
        echo "<pre>";
        var_dump($parameter);
         echo "<pre>";
    }
    
   static public function debugDie($parameter = false){
       self::Debug($parameter);
    die();
    }
    
    static public function redirect($url="/Atomic/views/SEIP107413/BookTitle/index.php"){
        header("Location:".$url);
    }
    
    static public function message($message  = null){
        if (is_null($message)) {
            $_message = self::getMessage();
            return $_message;
        }else {
            self::setMessage($message); 
        }
    }
    
    static private function getMessage(){
        $_message = $_SESSION['message'];
            $_SESSION['message'] = "";
            return $_message;
    }
    static private function setMessage($message){
        $_SESSION['message'] = $message;
    }
}

