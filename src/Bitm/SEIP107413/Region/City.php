<?php

namespace App\Bitm\SEIP107413\Region;
use App\Bitm\SEIP107413\Utility\Utility;

class City {
    public $id = "";
    public $name = "";
    public $city = "";
    public $created;
    public $modified;
    public $created_by;
    public $modified_by;
    public $deleted_by;
    
    public function __construct($data = false){
       
       if( is_array($data) && array_key_exists('id', $data) && !empty($data['id'])){
            $this->id = $data['id'];
        }
        $this->name = $data['name'];
        $this->city = $data['city'];
    }

    public function index(){
        
        $cities = array();
        
        $conn = mysql_connect("localhost", "root","") or die("Cannot connect data.");
        $lnk = mysql_select_db("atomicproject") or die("Cannot connect data.");
        
        $query = "SELECT * FROM `city` ";
        $result = mysql_query($query);
        
        while ($row = mysql_fetch_assoc($result)) {
            $cities[] = $row;
        }
        return $cities;
//        $row = mysql_fetch_array($result);
//        $row1 = mysql_fetch_array($result);
//        $row2 = mysql_fetch_array($result);
//        
//        Utility::debug($row);
//        Utility::debug($row1);
//        Utility::debugDie($row2);
    }
    public function create(){
        echo "I am create form";
    }
    public function store(){
       
         $conn = mysql_connect("localhost", "root","") or die("Cannot connect data.");
         $lnk = mysql_select_db("atomicproject") or die("Cannot connect data.");
        
         $query = "INSERT INTO `city` (`name`,`city`) VALUES ('".$this->name."','".$this->city."')";
         $result = mysql_query($query);
         
//          $result = "I am storing data for " .$this->title;
          //return $result;
        if ($result) {
            Utility::message ("you have storing data for ");
        }  else {
            Utility::message ("There is an error while saving data. please try again later.");
        }
        
        Utility::redirect('index.php');
//       echo  "I am storing data for " .$book->title;
//        .$book->title;
    }
    
    public function show($id=false){
        
        $conn = mysql_connect("localhost","root","") or die("Cannot connect database.");
        $lnk = mysql_select_db("atomicproject") or die("Cannot select database.");

        $query = "SELECT * FROM `city` WHERE id =".$id;
        $result = mysql_query($query);
        
        $row = mysql_fetch_assoc($result);
        
        return $row;
    }
    
    public function delete($id = null){
       
        if(is_null($id)){
            Utility::message('No id avaiable. Sorry !');
            return Utility::redirect('index.php');
        }
        
        $conn = mysql_connect("localhost","root","") or die("Cannot connect database.");
        $lnk = mysql_select_db("atomicproject") or die("Cannot select database.");

        $query = "DELETE FROM `atomicproject`.`city` WHERE `city`.`id` = ".$id;
        $result = mysql_query($query);
               
        if($result){
            Utility::message("Your City is deleted successfully.");
        }else{
            Utility::message(" Cannot delete.");
        }
        
        Utility::redirect('index.php');
    }
    
    public function edit(){
       echo  "I am editing form";
    }
    public function update(){
       $conn = mysql_connect("localhost","root","") or die("Cannot connect database.");
        $lnk = mysql_select_db("atomicproject") or die("Cannot select database.");
        
        $query = "UPDATE `atomicproject`.`city` SET `name` = '".$this->name."', `city` = '".$this->city."' WHERE `city`.`id` = ".$this->id;
        $result = mysql_query($query);
               
        if($result){
            Utility::message("Your City is updated successfully.");
        }else{
            Utility::message("There is an error while saving data. Please try again later.");
        }
        
        Utility::redirect('index.php');
    }
    
}
