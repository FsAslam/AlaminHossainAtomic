<?php

namespace App\Bitm\SEIP107413\Human;
use App\Bitm\SEIP107413\Utility\Utility;

class Gender {
    public $id = "";
    public $name = "";
    public $gender = "";
    public $created;
    public $modified;
    public $created_by;
    public $modified_by;
    public $deleted_by;
    
    public function __construct($data = false){
       
       if( is_array($data) && array_key_exists('id', $data) && !empty($data['id'])){
            $this->id = $data['id'];
        }
        $this->name = $data['name'];
        $this->gender = $data['gender'];
    }

    public function index(){
        
        $gens = array();
        
        $conn = mysql_connect("localhost", "root","") or die("Cannot connect data.");
        $lnk = mysql_select_db("atomicproject") or die("Cannot connect data.");
        
        $query = "SELECT * FROM `gender` ";
        $result = mysql_query($query);
        
        while ($row = mysql_fetch_assoc($result)) {
            $gens[] = $row;
        }
        return $gens;

    }
    public function create(){
        echo "I am create form";
    }
    public function store(){
       
         $conn = mysql_connect("localhost", "root","") or die("Cannot connect data.");
         $lnk = mysql_select_db("atomicproject") or die("Cannot connect data.");
        
         $query = "INSERT INTO `gender` (`name`,`gender`) VALUES ('".$this->name."','".$this->gender."')";
         $result = mysql_query($query);
         
        if ($result) {
            Utility::message ("you have storing data for ");
        }  else {
            Utility::message ("There is an error while saving data. please try again later.");
        }
        
        Utility::redirect('index.php');

    }
    
    public function show($id=false){
        
        $conn = mysql_connect("localhost","root","") or die("Cannot connect database.");
        $lnk = mysql_select_db("atomicproject") or die("Cannot select database.");

        $query = "SELECT * FROM `gender` WHERE id =".$id;
        $result = mysql_query($query);
        
        $row = mysql_fetch_assoc($result);
        
        return $row;
    }
    
    public function delete($id = null){
       
        if(is_null($id)){
            Utility::message('No id avaiable. Sorry !');
            return Utility::redirect('index.php');
        }
        
        $conn = mysql_connect("localhost","root","") or die("Cannot connect database.");
        $lnk = mysql_select_db("atomicproject") or die("Cannot select database.");

        $query = "DELETE FROM `atomicproject`.`gender` WHERE `gender`.`id` = ".$id;
        $result = mysql_query($query);
               
        if($result){
            Utility::message("your informstion is deleted successfully.");
        }else{
            Utility::message(" Cannot delete.");
        }
        
        Utility::redirect('index.php');
    }
    
    public function edit(){
       echo  "I am editing form";
    }
    public function update(){
       $conn = mysql_connect("localhost","root","") or die("Cannot connect database.");
        $lnk = mysql_select_db("atomicproject") or die("Cannot select database.");
        
        $query = "UPDATE `atomicproject`.`gender` SET `name` = '".$this->name."', `gender` = '".$this->gender."' WHERE `gender`.`id` = ".$this->id;
        $result = mysql_query($query);
               
        if($result){
            Utility::message("Informsation is updated successfully.");
        }else{
            Utility::message("There is an error while saving data. Please try again later.");
        }
        
        Utility::redirect('index.php');
    }
    
}
